package com.example.student1.application;

import android.util.Log;

/**
 * Created by student1 on 10.11.16.
 */
public class Dog extends Animal {
    private String name;
    private int imageId = R.drawable.dog;
    public Dog (String name) {
        this.name = name;
    }

    public void  eat() {
        Log.d("Mydog", "dog eat");

    }
    public void  sleep() {
        Log.d("Mydog", "dog sleep");
    }

    public String toString(){
        String parent = super.toString();
        return parent + " this is dog object";
    }
    public String getName(){
        return name;
    }
    public int getImageId(){
        return imageId;
    }
}
