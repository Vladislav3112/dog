package com.example.student1.application;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        layout = (LinearLayout)findViewById(R.id.layout);

        ArrayList<Animal> list = new ArrayList<>();{
        list.add(new Dog("Rex"));
            list.add(new Dog("Dog"));
        }
        for (Animal animal : list){
            ImageView image = new ImageView(this);
            TextView text = new TextView(this);

            image.setLayoutParams(new ViewGroup.LayoutParams(256,256));

            text.setText(animal.getName());
            image.setImageResource(animal.getImageId());

            layout.addView(image);
            layout.addView(text);
        }
    }


}
