package com.example.student1.application;

/**
 * Created by student1 on 10.11.16.
 */
public abstract class Animal {

    public abstract void eat();
    public abstract void sleep();
    public abstract int getImageId();
    public abstract String getName();
    public String toString(){

        return "this is animal object";
    }

}
